# linkedVMigrator

## Getting started

This application is used to migrate vmware VM's from one datastore cluster to another datastore cluster. 
This script works in linked vCenter mode, or non-linked vCenter mode.
Follow the format of the example.json and begin migrating!