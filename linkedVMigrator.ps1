# grabs the .json file and converts it to usable data
function Get-Json
{
    param
    (
        [Parameter()]
        $file
    )
    Get-Content $file -Raw | ConvertFrom-Json
}

function Get-Creds
{
    param (
        [Parameter()]
        [string]$credfile
    )

    $vcUsername = (Import-CliXml $credfile).UserName
    $vcPassword = (Import-CliXml $credfile).Password
    $vcCredential = New-Object System.Management.Automation.PSCredential ($vcUsername, $vcPassword)
    
    return $vcCredential
}

function Write-Log
{
    param (
        [Parameter(Mandatory=$true)] [string] $Message
    )
    Try {
        $LogDate = (Get-Date).ToString("MM-dd-yyyy")
        $LogFile = $logPath + "\Log" + $LogDate + ".txt"
        # Add Content to Log File
        $TimeStamp = (Get-Date).ToString("MM/dd/yyy HH:mm:ss:fff tt")
        $Line = "$TimeStamp - $Message"
        Add-Content -Path $LogFile -Value $Line
    } Catch {
        Write-Host -f Red "Error:" $_.Exception.Message
    }
    
}

function Connect-VC
{
	param
	(
		[Parameter(Mandatory, ValueFromPipeLine)]
		[string]$vcServer,
		
		[Parameter(Mandatory=$false)]
		[ValidateNotNull()]
	        [System.Management.Automation.PSCredential] 
			$vcCredential = [System.Management.Automation.PSCredential]::Empty
	)
	
    if ($vcCredential -eq $null) {
        $vcCredential = Get-Credential
    }

	# Establish vCenter connection with user credentials
 	Write-Host "Connecting to $vcServer as"$vcCredential.Username"..." -Foregroundcolor "Yellow" -NoNewLine

	Write-Log "Connecting to $vcServer..."
    # the -AllLinked parameter specifies these vcenter will be in linked mode allowing you to only login to one
	$connection = Connect-VIServer -Server $vcServer -Credential $vcCredential -AllLinked #-ErrorAction SilentlyContinue -WarningAction 0
	# If successful, execute operations
	If ($connection) 
	{
        	Write-Host "Success!`n" -Foregroundcolor "Green"
        	Write-Log "Success!"
        	#Return $True
	}
	# If connection to vCenter wasn't successful, tell me why
	Else
	{
        	Write-Host "Error connecting to $vcServer; Try again with correct user name & password!" -Foregroundcolor "Red"
        	Write-Log "Error connecting to $vcServer; Try again with correct user name & password!"
	}
	
}

function Get-DatastoreArray {
    param(
        [Parameter()]
        [string]$datastoreCluster
    )
    # Pulls an array of datastores from the related datastore cluster object
    $datastoreArr = Get-Datastore -RelatedObject $datastoreCluster
    if ($datastoreArr) {
        Write-Log "Successfully gathered datastoreArr for cluster $datastoreCluster"
    } else {
        Write-Log "Failed to gather datastoreArr for cluster $datastoreCluster"
    }

    return $datastoreArr
}

function Get-VMHostArray {
    param(
        [Parameter()]
        [VMware.VimAutomation.ViCore.Types.V1.DatastoreManagement.Datastore]$datastore
    )
    # Pulls all of the hosts attached to a given datastore
    $VMHostArray = Get-VMHost -Datastore $datastore
    if ($VMHostArray) {
        Write-Log "Successfully gathered VMHostArr for $datastore"
    } else {
        Write-Log "Failed to gather VMHostArr for $datastore"
    }

    return $VMHostArray
}

function Get-VMArray {
    param(
        [Parameter()]
        [System.Array]$datastoreArr
    )

    $vmArr = @()
    # Loops through each datastore appending the vms to an array
    foreach ($datastore in $datastoreArr) 
    {
        $vmArr += Get-VM -Datastore $datastore
    }

    return $vmArr
}

function Compare-Datastores {
    param(
        [Parameter()]
        [System.Array]$datastoreArr
    )

    # Loops through each datastore in the array and picks the one with the most free space
    $recommendDatastore = $datastoreArr[0]
    foreach ($datastore in $datastoreArr) {
        if ($datastore.FreeSpaceGB -gt $recommendDatastore.FreeSpaceGB) {
            $recommendDatastore = $datastore
        }
    }
    return $recommendDatastore
}

function Compare-VDPortGroups {
    param(
        [Parameter()]
        [VMware.VimAutomation.Vds.Types.V1.VDPortgroup]$VDPortGroup,
        [VMware.VimAutomation.ViCore.Impl.V1.Inventory.VMHostImpl]$VMHost
    )
    # Off VMs will sometimes not have an assigned VDPortGroup, this catches those VMs
    if ($VDPortGroup -eq $null) {
        return $VDPortGroup
    }

    # VDPortGroups should share the same name across vCenters, this pulls all VDPortGroups of the same name.
    # Then it loops through each VDPortGroups host data looking for the destination host returning the VDPortGroup with the host
    $VDPortGroupArr = Get-VDPortgroup -Name $VDPortgroup.Name
    foreach ($portGroup in $VDPortGroupArr) {
        foreach ($hostObj in $portGroup.ExtensionData.Host) {
            if ($hostObj.Value -eq $VMHost.ExtensionData.Summary.Host.Value) {
                return $portGroup
            }
        }
    }
}

function Compare-Folders {
    param(
        [Parameter()]
        [String]$sourceFolder
    )
    
    $parent, $child = $sourceFolder -split "/"
    # Based on our folder structure it's possible to have the same name fodler across vcenters
    # This function pulls all folders of the same name across all vcenters, then using the
    # parent folder, it determines which folder is the desired one
    $destinationFolderArr = Get-Folder -Name $child
    foreach ($folder in $destinationFolderArr) {
        if ($folder.Parent.Name -eq $parent) {
            return $folder
        }
    }
}

function Move-VMs {
    param(
        [Parameter()]
        [System.Array]$vmsArr,
        [System.Array]$destHostArr,
        [String]$folderName
    )
    
    # Loop through each VM one at a time in order to only migrate one at a time
    foreach ($vm in $vmsArr) {
        $sortVDPortgroup = @()
        $VDPortgroup = @()
        # Reset the Datastore array after every VM migration to get accurate allocated storage
        $datastoreArr = Get-DatastoreArray $fileContent.destination_datastorecluster
        $destinationFolder = Compare-Folders $folderName
        $networkAdapter = Get-NetworkAdapter -VM $vm
        # Sometimes VM's have multiple Network Adapters, it is crucial all Network Adapters are referenced in the migration
        if ($networkAdapter.Length -gt 1) {
            # When a VM has multiple Network Adapters is pulls an Array of VDPortGroups
            foreach ($network in $networkAdapter) {
                $VDPortgroup += Get-VDPortgroup -NetworkAdapter $network
            }
            foreach ($PortGroup in $VDPortGroup) {
                #Used to decide which VDPortGroup to migrate to, a sample host is needed to confirm the VDPortGroup
                $sortVDPortgroup += Compare-VDPortGroups $PortGroup $destHostArr[0]
            }
        } else {
            # One Network Adapter
            $VDPortgroup += Get-VDPortgroup -NetworkAdapter $networkAdapter
            $sortVDPortgroup += Compare-VDPortGroups $VDPortgroup[0] $destHostArr[0]
        }
        # vCenters datastore drs is not efficient and not aggressive enough to spread the resources properly, this func is why we reset the datastore after every VM
        $drsDatastore = Compare-Datastores $datastoreArr
        # Can't migrate a VM with a null Network Adapter or a null VDPortGroup, this migrates VMs based on if they were never assigned a Network Adapter or VDPortGroup
        if ($null -ne $sortVDPortgroup) {
            $moveLog = Move-VM -VM $vm -Datastore $drsDatastore -Destination $destHostArr[0] -InventoryLocation $destinationFolder -NetworkAdapter $networkAdapter -PortGroup $sortVDPortgroup
        } else {
            $moveLog = Move-VM -VM $vm -Datastore $drsDatastore -Destination $destHostArr[0] -InventoryLocation $destinationFolder
        }
        if ($moveLog) {
            Write-Log "Successfully migrated VM $moveLog"
        } else {
            Write-Log "Failed to migrate VM $vm"
            break
        }
    }
}

# parses the file from the arguments and pulls the necessary pieces of data
$filepath = Join-Path $PSScriptRoot $args[0]
$vcCreds = $null
if ($args.Length -eq 2) {
    $credfile = Join-Path $PSScriptRoot $args[1]
    $vcCreds = Get-Creds $credfile
}
$fileContent = Get-Json $filepath
$vc = $fileContent.vCenters
$logFolder = $fileContent.logsPath
$logPath = Join-Path $PSScriptRoot $logFolder

# establish connection to the linked vcenters
Connect-VC $vc $vcCreds

# Pull the datastores based on the name specified in the .json.
$destinationDatastoreArr = Get-DatastoreArray $fileContent.destination_datastorecluster
$sourceDatastoreArr = Get-DatastoreArray $fileContent.source_datastorecluster
# We specify destinationDatastoreArr[0] because our systems are set up where every datastore in a cluster will share the same hosts
$hostArr = Get-VMHostArray $destinationDatastoreArr[0] 
$vms = Get-VMArray $sourceDatastoreArr
Move-VMs $vms $hostArr $fileContent.destination_folder